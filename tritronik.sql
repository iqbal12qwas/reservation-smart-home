-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (arm64)
--
-- Host: 127.0.0.1    Database: tritronik
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `additional_facility`
--

DROP TABLE IF EXISTS `additional_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `additional_facility` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `price` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `additional_facility`
--

LOCK TABLES `additional_facility` WRITE;
/*!40000 ALTER TABLE `additional_facility` DISABLE KEYS */;
INSERT INTO `additional_facility` VALUES (1,'Breakfast',200000),(2,'Extrabed',50000),(3,'Non Smoking',20000),(4,'Smoking',50000),(5,'Top Floor/View City',100000);
/*!40000 ALTER TABLE `additional_facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `amount` decimal(38,0) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `data_additional` longtext NOT NULL,
  `data_approval` varchar(255) DEFAULT NULL,
  `data_room` longtext NOT NULL,
  `data_user` longtext NOT NULL,
  `is_payment` tinyint(1) NOT NULL,
  `proof_payment` varchar(255) DEFAULT NULL,
  `total_day` int NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (1,1300000,'2023-07-05 19:05:10.976000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]','{\"id\":3,\"name\":\"Bale2\",\"email\":\"bale2@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}','{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',2,'Gold-Frame-PNG.png',2,'2023-07-05 19:36:33.000000'),(2,1300000,'2023-07-07 20:48:45.887000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,2,'2023-07-07 20:48:45.887000'),(3,3300000,'2023-07-07 20:58:28.991000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":2,\"type\":\"Twin\",\"price\":1000000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-07-07 20:58:28.991000'),(4,3300000,'2023-07-07 20:59:18.437000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":2,\"type\":\"Twin\",\"price\":1000000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-07-07 20:59:18.437000'),(5,3300000,'2023-07-07 20:59:29.817000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":2,\"type\":\"Twin\",\"price\":1000000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-07-07 20:59:29.817000'),(6,3300000,'2023-07-07 21:00:27.086000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":2,\"type\":\"Twin\",\"price\":1000000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-07-07 21:00:27.086000'),(7,3300000,'2023-07-08 12:37:44.156000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":2,\"type\":\"Twin\",\"price\":1000000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-07-08 12:37:44.156000'),(8,3300000,'2023-07-08 12:39:03.181000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":2,\"type\":\"Twin\",\"price\":1000000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-07-08 12:39:03.181000'),(9,3300000,'2023-07-08 12:46:01.581000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]','{\"id\":3,\"name\":\"Bale2\",\"email\":\"bale2@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}','{\"id\":2,\"type\":\"Twin\",\"price\":1000000}','{\"id\":4,\"name\":\"Bale3\",\"email\":\"bale3@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',2,'Gold-Frame-PNG.png',3,'2023-07-08 13:18:45.000000'),(10,1800000,'2023-08-11 15:43:14.393000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 15:43:14.393000'),(11,1800000,'2023-08-11 15:49:26.978000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 15:49:26.978000'),(12,1800000,'2023-08-11 15:57:12.257000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 15:57:12.257000'),(13,1782000,'2023-08-11 15:58:03.145000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 15:58:03.145000'),(14,1799980,'2023-08-11 15:59:20.378000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 15:59:20.378000'),(15,1799980,'2023-08-11 16:04:24.669000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 16:04:24.669000'),(16,1799980,'2023-08-11 16:06:02.161000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 16:06:02.161000'),(17,1440000,'2023-08-11 17:25:04.490000','[{\"id\":1,\"description\":\"Breakfast\",\"price\":200000},{\"id\":5,\"description\":\"Top Floor/View City\",\"price\":100000}]',NULL,'{\"id\":1,\"type\":\"Single\",\"price\":500000}','{\"id\":1,\"name\":\"Bale\",\"email\":\"bale@gmail.com\",\"username\":null,\"password\":null,\"createdAt\":null,\"updatedAt\":null,\"roles\":[]}',0,NULL,3,'2023-08-11 17:25:04.490000');
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` enum('ROLE_EMPLOYEE','ROLE_USER') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_EMPLOYEE'),(2,'ROLE_USER'),(3,NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `price` int NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (1,500000,'Single'),(2,1000000,'Twin'),(3,1200000,'Deluxe'),(4,2000000,'Family');
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`),
  UNIQUE KEY `UKsb8bbouer5wak8vyiiy4pf2bx` (`username`),
  UNIQUE KEY `UKob8kqyqqgmefl0aco34akdtpe` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2023-07-05 18:43:01.087000','bale@gmail.com','Bale','$2a$10$HCsruJ/61Mlk.fK4XU.SZOjNxTnuVUGFoCWAe7kBfmmk4SzLyMJVy','2023-07-05 18:43:01.087000','bale'),(2,'2023-07-05 18:44:40.109000','bale1@gmail.com','Bale1','$2a$10$89gCVjP9oheQWbQKmbtMH.V93.f9tUqR6IQFAgWrcViPpGeQr.whK','2023-07-05 18:44:40.109000','bale1'),(3,'2023-07-05 18:44:56.397000','bale2@gmail.com','Bale2','$2a$10$8YLGCr/AQGcbnL49Ow/fwuFoaTVu/rdx3oqysNWGXpdq1rjJqmkpS','2023-07-05 18:44:56.397000','bale2'),(4,'2023-07-05 18:46:40.298000','bale3@gmail.com','Bale3','$2a$10$f2vitLhISnPwPQLtVkhQUeZ48pRH9JmvwZhNR9b1UwRBER6JS1sg6','2023-07-05 18:46:40.298000','bale3'),(5,'2023-07-05 18:46:51.060000','bale4@gmail.com','Bale4','$2a$10$c.kbwebqB6eBuKNT99iXfeCR95VP2Dkg8WWlIwDUY51AsLn16BUE2','2023-07-05 18:46:51.060000','bale4');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `user_id` bigint NOT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKrhfovtciq1l558cw6udg0h0d3` (`role_id`),
  CONSTRAINT `FK55itppkw3i07do3h7qoclqd4k` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKrhfovtciq1l558cw6udg0h0d3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (3,1),(2,2),(4,2),(5,2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tritronik'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-08-11 17:50:57
