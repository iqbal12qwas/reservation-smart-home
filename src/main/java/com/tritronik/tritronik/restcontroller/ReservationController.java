package com.tritronik.tritronik.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tritronik.tritronik.dto.ApprovalDto;
import com.tritronik.tritronik.dto.ReservationDto;
import com.tritronik.tritronik.kafka.producer.ReservationProducer;
import com.tritronik.tritronik.services.ReservationService;

/**
 * ReservationAPI
 */
@RestController
@RequestMapping(path = "/api/v1/reservation", produces = "application/json")
@CrossOrigin(origins = "*")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationProducer reservationProducer;

    @Value("${spring.kafka.consumer.group-id}")
    String kafkaGroupId;

    @Value("${tritronik.kafka.post.reservation}")
    String postReservation;

    @Value("${tritronik.kafka.put.approved-reservation}")
    String putApprovedReservation;

    public ReservationController(ReservationService reservationService, ReservationProducer reservationProducer) {
        this.reservationService = reservationService;
        this.reservationProducer = reservationProducer;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("hasRole('USER')")
    public Object save(@RequestBody ReservationDto reservationDto) {
        // Using Kafka
        return this.reservationProducer.postReservation(postReservation, kafkaGroupId, reservationDto);
    }

    @PostMapping("/upload-file")
    @PreAuthorize("hasRole('USER')")
    public Object uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id) {
        return this.reservationService.uploadFile(file, id);
    }

    @PutMapping("/approved")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Object approvedReservation(@RequestBody ApprovalDto approvalDto) {
        // Using Kafka
        return this.reservationProducer.putApprovedReservation(putApprovedReservation, kafkaGroupId, approvalDto);
    }
}
