package com.tritronik.tritronik.restcontroller;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tritronik.tritronik.dto.ResponseDto;
import com.tritronik.tritronik.dto.request.LoginRequest;
import com.tritronik.tritronik.dto.request.SignupRequest;
import com.tritronik.tritronik.dto.response.JwtResponse;
import com.tritronik.tritronik.entity.ERole;
import com.tritronik.tritronik.entity.Role;
import com.tritronik.tritronik.entity.User;
import com.tritronik.tritronik.repository.RoleRepository;
import com.tritronik.tritronik.repository.UserRepository;
import com.tritronik.tritronik.security.jwt.JwtUtils;
import com.tritronik.tritronik.security.service.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public Object authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        ResponseDto responseDto = new ResponseDto();
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(item -> item.getAuthority())
                    .collect(Collectors.toList());

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("User signin successfully!");
            responseDto.setData(new JwtResponse(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    roles));
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    @PostMapping("/signup")
    public Object registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        ResponseDto responseDto = new ResponseDto();
        try {
            if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            }

            if (userRepository.existsByEmail(signUpRequest.getEmail())) {
                throw new Exception("Email is already in use!");
            }

            // Create new user's account
            User user = new User(
                    signUpRequest.getName(),
                    signUpRequest.getEmail(),
                    signUpRequest.getUsername(),
                    encoder.encode(signUpRequest.getPassword()),
                    new Date(),
                    new Date());

            Set<String> strRoles = signUpRequest.getRole();
            Set<Role> roles = new HashSet<>();

            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "employee":
                            Role adminRole = roleRepository.findByName(ERole.ROLE_EMPLOYEE)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);

                            break;
                        default:
                            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(userRole);
                    }
                });
            }

            user.setRoles(roles);
            userRepository.save(user);

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("User registered successfully!");
            responseDto.setData(user);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }
}