package com.tritronik.tritronik.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tritronik.tritronik.dto.DiscountDto;
import com.tritronik.tritronik.kafka.producer.DiscountProducer;
import com.tritronik.tritronik.services.DiscountService;

/**
 * DiscountAPI
 */
@RestController
@RequestMapping(path = "/api/v1/discount", produces = "application/json")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('USER')")
public class DiscountController {

    @Autowired
    private DiscountService discountService;

    @Autowired
    private DiscountProducer discountProducer;

    @Value("${spring.kafka.consumer.group-id}")
    String kafkaGroupId;

    @Value("${tritronik.kafka.post.discount}")
    String postDiscount;

    @Value("${tritronik.kafka.put.discount}")
    String putDiscount;

    public DiscountController(DiscountService discountService, DiscountProducer discountProducer) {
        this.discountService = discountService;
        this.discountProducer = discountProducer;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("hasRole('USER')")
    public Object save(@RequestBody DiscountDto discountDto) {
        // Using Kafka
        return this.discountProducer.postDiscount(postDiscount, kafkaGroupId, discountDto);
    }

    @PutMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("hasRole('USER')")
    public Object put(@RequestBody DiscountDto discountDto) {
        // Using Kafka
        return this.discountProducer.putDiscount(putDiscount, kafkaGroupId, discountDto);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Object getDiscountAll() {
        return this.discountService.getDiscountAll();
    }
}
