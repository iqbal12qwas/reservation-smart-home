package com.tritronik.tritronik.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tritronik.tritronik.services.AdditionalFacilityService;

/**
 * AdditionalFacilityAPI
 */
@RestController
@RequestMapping(path = "/api/v1/additional-facility", produces = "application/json")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('USER')")
public class AdditionalFacilityController {

    @Autowired
    private AdditionalFacilityService additionalFacilityService;

    public AdditionalFacilityController(AdditionalFacilityService additionalFacilityService) {
        this.additionalFacilityService = additionalFacilityService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Object getAdditionalFacilityAll() {
        return this.additionalFacilityService.getAdditionalFacilityAll();
    }
}
