package com.tritronik.tritronik.dto;

public class DataAdditionalFacilityDto {

    private Long id;
    private String description;
    private Integer price;

    public DataAdditionalFacilityDto() {

    }

    public DataAdditionalFacilityDto(Long id, String description, Integer price) {
        this.id = id;
        this.description = description;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
