package com.tritronik.tritronik.dto;

import java.util.Date;

public class DiscountDto {

    private String id;
    private String name;
    private Date expired;
    private Integer nominal;
    private String type;

    public DiscountDto() {
    }

    public DiscountDto(String id, String name, Date expired, Integer nominal, String type) {
        this.id = id;
        this.name = name;
        this.expired = expired;
        this.nominal = nominal;
        this.type = type;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpired() {
        return this.expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    public Integer getNominal() {
        return this.nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
