package com.tritronik.tritronik.dto;

import com.tritronik.tritronik.entity.User;

public class ApprovalDto {

    private Long id;
    private User dataEmployee;

    public ApprovalDto() {

    }

    public ApprovalDto(Long id, User dataEmployee) {
        this.id = id;
        this.dataEmployee = dataEmployee;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getDataEmployee() {
        return this.dataEmployee;
    }

    public void setDataEmployee(User dataEmployee) {
        this.dataEmployee = dataEmployee;
    }

}
