package com.tritronik.tritronik.dto;

public class RoomDto {
    private Long id;
    private String type;
    private Integer price;

    public RoomDto() {

    }

    public RoomDto(Long id, String type, Integer price) {
        this.id = id;
        this.type = type;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
