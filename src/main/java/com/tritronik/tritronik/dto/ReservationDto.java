package com.tritronik.tritronik.dto;

import java.math.BigInteger;
import java.util.List;

import com.tritronik.tritronik.entity.AdditionalFacility;
import com.tritronik.tritronik.entity.Room;
import com.tritronik.tritronik.entity.User;

public class ReservationDto {

    private Long id;
    private User dataUser;
    private Room dataRoom;
    private List<AdditionalFacility> dataAdditionalFacility;
    private Integer totalDay;
    private BigInteger amount;
    private Integer isPayment;
    private String proofPayment;
    private String idDiscount;

    public ReservationDto() {
    }

    public ReservationDto(Long id, User dataUser, Room dataRoom, List<AdditionalFacility> dataAdditionalFacility,
            Integer totalDay, BigInteger amount, Integer isPayment, String proofPayment, String idDiscount) {
        this.id = id;
        this.dataUser = dataUser;
        this.dataRoom = dataRoom;
        this.dataAdditionalFacility = dataAdditionalFacility;
        this.totalDay = totalDay;
        this.amount = amount;
        this.isPayment = isPayment;
        this.proofPayment = proofPayment;
        this.idDiscount = idDiscount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getDataUser() {
        return dataUser;
    }

    public void setDataUser(User dataUser) {
        this.dataUser = dataUser;
    }

    public Room getDataRoom() {
        return dataRoom;
    }

    public void setDataRoom(Room dataRoom) {
        this.dataRoom = dataRoom;
    }

    public List<AdditionalFacility> getDataAdditionalFacility() {
        return dataAdditionalFacility;
    }

    public void setDataAdditionalFacility(List<AdditionalFacility> dataAdditionalFacility) {
        this.dataAdditionalFacility = dataAdditionalFacility;
    }

    public Integer getTotalDay() {
        return totalDay;
    }

    public void setTotalDay(Integer totalDay) {
        this.totalDay = totalDay;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public Integer getIsPayment() {
        return isPayment;
    }

    public void setIsPayment(Integer isPayment) {
        this.isPayment = isPayment;
    }

    public String getProofPayment() {
        return proofPayment;
    }

    public void setProofPayment(String proofPayment) {
        this.proofPayment = proofPayment;
    }

    public String getIdDiscount() {
        return idDiscount;
    }

    public void setIdDiscount(String idDiscount) {
        this.idDiscount = idDiscount;
    }

}
