package com.tritronik.tritronik.services;

public interface RoomService {

    Object getRoomById(Long id);

    Object getRoomAll();
}
