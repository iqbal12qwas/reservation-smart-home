package com.tritronik.tritronik.services;

import org.springframework.web.multipart.MultipartFile;

import com.tritronik.tritronik.dto.ApprovalDto;
import com.tritronik.tritronik.dto.ReservationDto;

public interface ReservationService {

    Object save(ReservationDto reservationDto);

    Object uploadFile(MultipartFile file, Long id);

    Object approvedReservation(ApprovalDto approvalDto);
}
