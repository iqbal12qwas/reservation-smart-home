package com.tritronik.tritronik.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tritronik.tritronik.dto.ResponseDto;
import com.tritronik.tritronik.repository.RoomRepository;
import com.tritronik.tritronik.services.RoomService;

@Service
public class RoomImplementation implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    public RoomImplementation(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public Object getRoomAll() {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(this.roomRepository.findAll());
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    @Override
    public Object getRoomById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(this.roomRepository.findById(id));
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

}
