package com.tritronik.tritronik.services.implementation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tritronik.tritronik.dto.ApprovalDto;
import com.tritronik.tritronik.dto.FileUploadDto;
import com.tritronik.tritronik.dto.ReservationDto;
import com.tritronik.tritronik.dto.ResponseDto;
import com.tritronik.tritronik.entity.AdditionalFacility;
import com.tritronik.tritronik.entity.Reservation;
import com.tritronik.tritronik.entity.Role;
import com.tritronik.tritronik.entity.Room;
import com.tritronik.tritronik.entity.User;
import com.tritronik.tritronik.entity.mongodb.Discount;
import com.tritronik.tritronik.property.FileStorageProperties;
import com.tritronik.tritronik.repository.ReservationRepository;
import com.tritronik.tritronik.repository.RoleRepository;
import com.tritronik.tritronik.repository.RoomRepository;
import com.tritronik.tritronik.repository.UserRepository;
import com.tritronik.tritronik.repository.mongodb.DiscountRepository;
import com.tritronik.tritronik.services.ReservationService;

import org.springframework.util.StringUtils;

@Service
public class ReservationImplementation implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;
    private UserRepository userRepository;
    private RoomRepository roomRepository;
    private RoleRepository roleRepository;
    private DiscountRepository discountRepository;
    private final Path fileUploadLocation;

    public ReservationImplementation(ReservationRepository reservationRepository, UserRepository userRepository,
            RoomRepository roomRepository, RoleRepository roleRepository, FileStorageProperties fileStorageProperties,
            DiscountRepository discountRepository) {
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
        this.roleRepository = roleRepository;
        this.discountRepository = discountRepository;

        this.fileUploadLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileUploadLocation);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Cannot create the directory where you want to the uploaded the files will be kept.", e);
        }
    }

    @Override
    public Object save(ReservationDto reservationDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            User dataUser = reservationDto.getDataUser();
            Optional<User> existingDataUser = this.userRepository.findById(dataUser.getId());
            if (existingDataUser.isEmpty()) {
                throw new Exception("User Not Found!");
            }

            Room dataRoom = reservationDto.getDataRoom();
            Optional<Room> existingDataRoom = this.roomRepository.findById(dataRoom.getId());
            if (existingDataRoom.isEmpty()) {
                throw new Exception("Room Not Found!");
            }

            // Calculate Amount
            Integer totalAdditional = 0;
            for (AdditionalFacility af : reservationDto.getDataAdditionalFacility()) {
                totalAdditional += af.getPrice();
            }
            Integer totalReserve = dataRoom.getPrice() * reservationDto.getTotalDay();
            BigInteger amount = BigInteger.valueOf(totalReserve + totalAdditional);

            // Discount If Exist
            BigInteger finalAmount = new BigInteger("0");
            if (!reservationDto.getIdDiscount().isEmpty()) {
                Optional<Discount> dataDiscountExisting = this.discountRepository
                        .findById(reservationDto.getIdDiscount());

                String typeDiscount = dataDiscountExisting.stream().findAny().get().getType();
                Integer nominal = Integer.valueOf(dataDiscountExisting.stream().findAny().get().getNominal());
                BigInteger biNominal = BigInteger.valueOf(nominal);
                if (typeDiscount.equals("percentage")) {
                    BigDecimal oneHundred = new BigDecimal("100");
                    BigDecimal bcAmount = new BigDecimal(amount);
                    BigDecimal bcNominal = new BigDecimal(biNominal);
                    BigDecimal nominalDiscount = bcNominal.divide(oneHundred);
                    BigInteger totalDiscount = bcAmount.multiply(nominalDiscount).toBigInteger();
                    finalAmount = amount.subtract(totalDiscount);
                } else if (typeDiscount.equals("price")) {
                    finalAmount = amount.subtract(biNominal);
                }
            }

            ObjectMapper Obj = new ObjectMapper();
            Reservation reservation = new Reservation();
            reservation.setDataUser(Obj.writeValueAsString(reservationDto.getDataUser()));
            reservation.setDataRoom(Obj.writeValueAsString(reservationDto.getDataRoom()));
            reservation.setDataAdditional(Obj.writeValueAsString(reservationDto.getDataAdditionalFacility()));
            reservation.setTotalDay(reservationDto.getTotalDay());
            reservation.setAmount(finalAmount);
            reservation.setIsPayment(0);
            reservation.setCreatedAt(new Date());
            reservation.setUpdatedAt(new Date());
            this.reservationRepository.save(reservation);

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(reservation);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    @Override
    public Object uploadFile(MultipartFile file, Long id) {
        ResponseDto responseDto = new ResponseDto();
        try {
            Optional<Reservation> dataExisting = this.reservationRepository.findById(id);
            if (dataExisting.isEmpty()) {
                throw new Exception("Reservation Not Found!");
            }

            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            if (fileName.contains("..")) {
                throw new RuntimeException("Sorry! File name is containing invalid path sequence " + fileName);
            }

            Path targetLocation = this.fileUploadLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            FileUploadDto fileUploadDto = new FileUploadDto();
            fileUploadDto.setFileName(fileName);
            fileUploadDto.setSize(file.getSize());
            fileUploadDto.setDownloadUri(targetLocation + "");

            // Update Data
            this.reservationRepository.updateFileReservationById(fileName, id);

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(fileUploadDto);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    @Override
    public Object approvedReservation(ApprovalDto approvalDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            Optional<Reservation> dataReservationExisting = this.reservationRepository.findById(approvalDto.getId());
            if (dataReservationExisting.isEmpty()) {
                throw new Exception("Reservation Not Found!");
            }

            User dataEmployee = approvalDto.getDataEmployee();
            Optional<Role> existingDataEmployee = this.roleRepository.checkAccountEmployee(dataEmployee.getId());
            if (existingDataEmployee.isEmpty()) {
                throw new Exception("Employee Not Found!");
            }
            Integer isPayment = dataReservationExisting.stream().findAny().get().getIsPayment();
            if (!isPayment.equals(1)) {
                throw new Exception("Is Payment Not Valid!");
            }

            ObjectMapper Obj = new ObjectMapper();
            String dataApproval = Obj.writeValueAsString(approvalDto.getDataEmployee());

            // Update Data
            this.reservationRepository.updateApprovalById(dataApproval, approvalDto.getId());

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(approvalDto);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }
}
