package com.tritronik.tritronik.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tritronik.tritronik.dto.ResponseDto;
import com.tritronik.tritronik.repository.AdditionalFacilityRepository;
import com.tritronik.tritronik.services.AdditionalFacilityService;

@Service
public class AdditionalFacilityImplementation implements AdditionalFacilityService {

    @Autowired
    private AdditionalFacilityRepository additionalFacilityRepository;

    public AdditionalFacilityImplementation(AdditionalFacilityRepository additionalFacilityRepository) {
        this.additionalFacilityRepository = additionalFacilityRepository;
    }

    @Override
    public Object getAdditionalFacilityAll() {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(this.additionalFacilityRepository.findAll());
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }
}
