package com.tritronik.tritronik.services.implementation;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tritronik.tritronik.dto.ResponseDto;
import com.tritronik.tritronik.dto.UserDto;
import com.tritronik.tritronik.entity.User;
import com.tritronik.tritronik.repository.UserRepository;
import com.tritronik.tritronik.services.UserService;

@Service
public class UserImplementation implements UserService {

    @Autowired
    private UserRepository userRepository;

    public UserImplementation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Object save(UserDto userDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            User existingData = findByEmail(userDto.getEmail());
            if (existingData != null) {
                throw new Exception("Email Exist!");
            }

            // Hashing
            String password = hashingPassword(userDto.getPassword());

            User user = new User();
            user.setName(userDto.getName());
            user.setEmail(userDto.getEmail());
            user.setPassword(password);
            user.setCreatedAt(new Date());
            user.setUpdatedAt(new Date());
            this.userRepository.save(user);

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(userDto);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    @Override
    public User findByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    private String hashingPassword(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(text.getBytes(StandardCharsets.UTF_8));
            byte[] digest = md.digest();
            String password = String.format("%064x", new BigInteger(1, digest));
            return password;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
