package com.tritronik.tritronik.services.implementation;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tritronik.tritronik.dto.DiscountDto;
import com.tritronik.tritronik.dto.ResponseDto;
import com.tritronik.tritronik.entity.mongodb.Discount;
import com.tritronik.tritronik.repository.mongodb.DiscountRepository;
import com.tritronik.tritronik.services.DiscountService;

@Service
public class DiscountImplementation implements DiscountService {

    @Autowired
    private DiscountRepository discountRepository;

    public DiscountImplementation(DiscountRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

    @Override
    public Object save(DiscountDto discountDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            Discount discount = new Discount();
            discount.setName(discountDto.getName());
            discount.setNominal(discountDto.getNominal());
            discount.setExpired(discountDto.getExpired());
            discount.setType(discountDto.getType());
            discount.setCreatedAt(new Date());
            discount.setUpdatedAt(new Date());
            this.discountRepository.save(discount);

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(discount);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    @Override
    public Object update(DiscountDto discountDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            Optional<Discount> dataDiscountExisting = this.discountRepository.findById(discountDto.getId());
            if (dataDiscountExisting.isEmpty()) {
                throw new Exception("Reservation Not Found!");
            }

            // Update Data
            Discount discount = dataDiscountExisting.get();

            discount.setName(discountDto.getName());
            discount.setNominal(discountDto.getNominal());
            discount.setExpired(discountDto.getExpired());
            discount.setType(discountDto.getType());
            discount.setUpdatedAt(new Date());
            this.discountRepository.save(discount);

            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(discount);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    @Override
    public Object getDiscountAll() {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(this.discountRepository.findAll());
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }
}
