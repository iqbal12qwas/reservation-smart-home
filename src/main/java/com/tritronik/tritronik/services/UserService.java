package com.tritronik.tritronik.services;

import com.tritronik.tritronik.dto.UserDto;
import com.tritronik.tritronik.entity.User;

public interface UserService {

    Object save(UserDto user);

    User findByEmail(String email);
}
