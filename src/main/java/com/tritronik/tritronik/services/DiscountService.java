package com.tritronik.tritronik.services;

import com.tritronik.tritronik.dto.DiscountDto;

public interface DiscountService {

    Object save(DiscountDto discountDto);

    Object update(DiscountDto discountDto);

    Object getDiscountAll();

}
