package com.tritronik.tritronik.entity.mongodb;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "discounts")
public class Discount {
    @Id
    private String id;

    private String name;
    private Date expired;
    private Integer nominal;
    private String type;
    private Date createdAt;
    private Date updatedAt;

    public Discount() {

    }

    public Discount(String id, String name, Date expired, Integer nominal, String type, Date createdAt,
            Date updatedAt) {
        this.id = id;
        this.name = name;
        this.expired = expired;
        this.nominal = nominal;
        this.type = type;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpired() {
        return this.expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    public Integer getNominal() {
        return this.nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Document [id=" + id + ", name=" + name + ", expired=" + expired + ", nominal=" + nominal
                + ", type=" + type + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
    }
}
