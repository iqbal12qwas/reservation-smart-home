package com.tritronik.tritronik.entity;

import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

@Data
@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "data_user", nullable = false, columnDefinition = "LONGTEXT")
    private String dataUser;

    @Column(name = "data_room", nullable = false, columnDefinition = "LONGTEXT")
    private String dataRoom;

    @Column(name = "data_additional", nullable = false, columnDefinition = "LONGTEXT")
    private String dataAdditional;

    @Column(name = "total_day", nullable = false)
    private Integer totalDay;

    @Column(name = "amount", nullable = false)
    private BigInteger amount;

    /*
     * 0 = Reservation
     * 1 = Upload File
     * 2 = Approved
     */
    @Column(name = "is_payment", nullable = false, columnDefinition = "TINYINT(1)")
    private Integer isPayment;

    @Column(name = "proof_payment")
    private String proofPayment;

    @Column(name = "data_approval")
    private String dataApproval;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;

    public Reservation() {

    }

    public Reservation(Long id, String dataUser, String dataRoom, String dataAdditional, Integer totalDay,
            BigInteger amount, Integer isPayment, String proofPayment, String dataApproval, Date createdAt,
            Date updatedAt) {
        this.id = id;
        this.dataUser = dataUser;
        this.dataRoom = dataRoom;
        this.dataAdditional = dataAdditional;
        this.totalDay = totalDay;
        this.amount = amount;
        this.isPayment = isPayment;
        this.proofPayment = proofPayment;
        this.dataApproval = dataApproval;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataUser() {
        return this.dataUser;
    }

    public void setDataUser(String dataUser) {
        this.dataUser = dataUser;
    }

    public String getDataRoom() {
        return this.dataRoom;
    }

    public void setDataRoom(String dataRoom) {
        this.dataRoom = dataRoom;
    }

    public String getDataAdditional() {
        return this.dataAdditional;
    }

    public void setDataAdditional(String dataAdditional) {
        this.dataAdditional = dataAdditional;
    }

    public Integer getTotalDay() {
        return this.totalDay;
    }

    public void setTotalDay(Integer totalDay) {
        this.totalDay = totalDay;
    }

    public BigInteger getAmount() {
        return this.amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public Integer getIsPayment() {
        return this.isPayment;
    }

    public void setIsPayment(Integer isPayment) {
        this.isPayment = isPayment;
    }

    public String getProofPayment() {
        return this.proofPayment;
    }

    public void setProofPayment(String proofPayment) {
        this.proofPayment = proofPayment;
    }

    public String getDataApproval() {
        return this.dataApproval;
    }

    public void setDataApproval(String dataApproval) {
        this.dataApproval = dataApproval;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
