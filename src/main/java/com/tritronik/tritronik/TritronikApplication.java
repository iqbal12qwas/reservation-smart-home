package com.tritronik.tritronik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TritronikApplication {

	public static void main(String[] args) {
		SpringApplication.run(TritronikApplication.class, args);
	}

}
