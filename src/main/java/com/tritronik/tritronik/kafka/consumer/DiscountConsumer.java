package com.tritronik.tritronik.kafka.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tritronik.tritronik.dto.DiscountDto;
import com.tritronik.tritronik.services.DiscountService;

import jakarta.transaction.Transactional;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Transactional
public class DiscountConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    DiscountService discountService;

    @KafkaListener(topics = "4igc0qsg-tritronik.kafka.post.discount", groupId = "tritronik")
    public void postDiscount(String brandJSON) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            DiscountDto discountDto = mapper.readValue(brandJSON, DiscountDto.class);
            discountService.save(discountDto);
        } catch (Exception e) {
            logger.error("An error occurred! '{}'", e.getMessage());
        }
    }

    @KafkaListener(topics = "4igc0qsg-tritronik.kafka.put.discount", groupId = "tritronik")
    public void putDiscount(String brandJSON) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            DiscountDto discountDto = mapper.readValue(brandJSON, DiscountDto.class);
            discountService.update(discountDto);
        } catch (Exception e) {
            logger.error("An error occurred! '{}'", e.getMessage());
        }
    }

}
