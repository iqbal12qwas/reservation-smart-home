package com.tritronik.tritronik.kafka.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tritronik.tritronik.dto.ApprovalDto;
import com.tritronik.tritronik.dto.ReservationDto;
import com.tritronik.tritronik.services.ReservationService;

import jakarta.transaction.Transactional;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Transactional
public class ReservationConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    ReservationService reservationService;

    @KafkaListener(topics = "4igc0qsg-tritronik.kafka.post.reservation", groupId = "tritronik")
    public void processReservation(String brandJSON) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ReservationDto reservationDto = mapper.readValue(brandJSON, ReservationDto.class);
            reservationService.save(reservationDto);
        } catch (Exception e) {
            logger.error("An error occurred! '{}'", e.getMessage());
        }
    }

    @KafkaListener(topics = "4igc0qsg-tritronik.kafka.put.approved-reservation", groupId = "tritronik")
    public void processApprovedReservation(String brandJSON) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ApprovalDto approvalDto = mapper.readValue(brandJSON, ApprovalDto.class);
            reservationService.approvedReservation(approvalDto);
        } catch (Exception e) {
            logger.error("An error occurred! '{}'", e.getMessage());
        }
    }

}
