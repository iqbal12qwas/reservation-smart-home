package com.tritronik.tritronik.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tritronik.tritronik.dto.ApprovalDto;
import com.tritronik.tritronik.dto.ReservationDto;
import com.tritronik.tritronik.dto.ResponseDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReservationProducer {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public Object postReservation(String topic, String groupId, ReservationDto reservationDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            ObjectMapper mapper = new ObjectMapper();
            kafkaTemplate.send(topic, groupId, mapper.writeValueAsString(reservationDto));
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(reservationDto);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    public Object putApprovedReservation(String topic, String groupId, ApprovalDto approvalDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            ObjectMapper mapper = new ObjectMapper();
            kafkaTemplate.send(topic, groupId, mapper.writeValueAsString(approvalDto));
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(approvalDto);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

}
