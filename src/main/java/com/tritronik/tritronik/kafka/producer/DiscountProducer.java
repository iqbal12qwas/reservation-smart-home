package com.tritronik.tritronik.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tritronik.tritronik.dto.DiscountDto;
import com.tritronik.tritronik.dto.ResponseDto;

@Service
public class DiscountProducer {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public Object postDiscount(String topic, String groupId, DiscountDto discountDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            ObjectMapper mapper = new ObjectMapper();
            kafkaTemplate.send(topic, groupId, mapper.writeValueAsString(discountDto));
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(discountDto);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }

    public Object putDiscount(String topic, String groupId, DiscountDto discountDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            ObjectMapper mapper = new ObjectMapper();
            kafkaTemplate.send(topic, groupId, mapper.writeValueAsString(discountDto));
            responseDto.setCode(200);
            responseDto.setSuccess(true);
            responseDto.setMessage("Success");
            responseDto.setData(discountDto);
            return responseDto;
        } catch (Exception e) {
            responseDto.setCode(403);
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
            return responseDto;
        }
    }
}
