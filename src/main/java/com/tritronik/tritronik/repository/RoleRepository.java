package com.tritronik.tritronik.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tritronik.tritronik.entity.ERole;
import com.tritronik.tritronik.entity.Role;

/**
 * RoleRepository
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERole name);

    @Query(value = "select * FROM role AS r JOIN user_roles AS ur ON ur.role_id = r.id WHERE ur.user_id = ?1 AND r.id = 1", nativeQuery = true)
    Optional<Role> checkAccountEmployee(Long id);
}
