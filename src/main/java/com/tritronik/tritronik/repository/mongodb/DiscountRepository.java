package com.tritronik.tritronik.repository.mongodb;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tritronik.tritronik.entity.mongodb.Discount;

import java.util.List;

public interface DiscountRepository extends MongoRepository<Discount, String> {
    List<Discount> findByName(String title);
}
