package com.tritronik.tritronik.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tritronik.tritronik.entity.Room;

/**
 * RoomRepository
 */
public interface RoomRepository extends JpaRepository<Room, Long> {

}
