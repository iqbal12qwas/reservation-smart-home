package com.tritronik.tritronik.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tritronik.tritronik.entity.AdditionalFacility;

/**
 * AdditionalFacilityRepository
 */
public interface AdditionalFacilityRepository extends JpaRepository<AdditionalFacility, Long> {

}
