package com.tritronik.tritronik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tritronik.tritronik.entity.Reservation;

import jakarta.transaction.Transactional;

/**
 * ReservationRepository
 */
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE reservation SET proof_payment =:proofPayment, is_payment = 1, updated_at = NOW() WHERE id = :id", nativeQuery = true)
    void updateFileReservationById(@Param("proofPayment") String proofPayment, @Param("id") Long id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE reservation SET data_approval =:dataApproval, is_payment = 2, updated_at = NOW() WHERE id = :id", nativeQuery = true)
    void updateApprovalById(@Param("dataApproval") String dataApproval, @Param("id") Long id);
}
